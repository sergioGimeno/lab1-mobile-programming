package com.example.sergiogimeno.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class A1 extends AppCompatActivity {
    Spinner spinner;
    int spinnerSelection;
    int selected;
    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        loadData();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1);
        final EditText editText = findViewById(R.id.editTextA1);
        Button nextActivityButton = findViewById(R.id.buttonToA2);
        nextActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editText.getText().toString();
                Intent intent = new Intent(getApplicationContext(),a2.class);
                intent.putExtra("myname",name);
                startActivity(intent);
            }
        });
        spinner = findViewById(R.id.spinner);
        adapter = ArrayAdapter.createFromResource(this,R.array.biq_saq,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(selected);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getBaseContext(),adapterView.getItemAtPosition(i)+ " selected",Toast.LENGTH_SHORT).show();
                spinnerSelection= i;


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    protected void onStop()
    {
        super.onStop();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("selection",spinnerSelection);
        editor.apply();
    }
    private void loadData()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        selected= prefs.getInt("selection",0);
        editor.apply();
    }

    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }
}
