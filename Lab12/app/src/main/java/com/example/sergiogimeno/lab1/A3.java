package com.example.sergiogimeno.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class A3 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);

        final Intent intentA2= getIntent();
        final String name = intentA2.getStringExtra("myname");

        final EditText editText = findViewById(R.id.editTextA2);
        Button previousActivityButton =  findViewById(R.id.buttonBackA2);
        previousActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editText.getText().toString();
                Intent intentA3 = new Intent(getApplicationContext(),a2.class);
                intentA3.putExtra("mytext",text);
                intentA3.putExtra("myname",name);
                startActivity(intentA3);
            }
        });
    }


    @Override
    public void onBackPressed()
    {
        Intent intentToA2= new Intent(getApplicationContext(),a2.class);
        startActivity(intentToA2);
    }
}
