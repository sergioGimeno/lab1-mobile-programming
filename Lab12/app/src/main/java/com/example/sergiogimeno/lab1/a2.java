package com.example.sergiogimeno.lab1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class a2 extends AppCompatActivity {
    String t2,t1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        loadData();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);
        TextView textView = findViewById(R.id.textViewA2);
        TextView textViewfromA3 = findViewById(R.id.textViewFromA3);
        Intent intent = getIntent();
        Intent intentA3= getIntent();
        String text = intentA3.getStringExtra("mytext");
        if(text!=null)
        {
            t2 = "From " + text;
        }
        else
        {
            t2="From ";
        }

        // Don't know why i should declare this variable final, but if i don't there is an error.
        final String name = intent.getStringExtra("myname");
        if(name!=null)
        {
            t1 = "Hello " + name;
        }
        else
        {
            t1="Hello ";
        }


        textView.setText(t1);
        textViewfromA3.setText(t2);

        Button nextButton= (Button) findViewById(R.id.buttonToA3);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentToA3= new Intent(getApplicationContext(),A3.class);
                intentToA3.putExtra("myname",name);
                startActivity(intentToA3);
            }
        });


    }

    protected void onStop()
    {
        super.onStop();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("textT1",t1);
        editor.putString("textT2",t2);
        editor.commit();
    }
    private void loadData()
    {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        t1= prefs.getString("textT1",t2);
        t2= prefs.getString("textT2",t2);
        editor.commit();
    }

    @Override
    public void onBackPressed()
    {
        Intent intentToA1= new Intent(getApplicationContext(),A1.class);
        startActivity(intentToA1);
    }
}
